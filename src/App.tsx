import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ToDoApp from "./components/ToDoApp";
import PendingToDoList from "./components/PendingToDoList";
import CompletedToDoList from "./components/CompletedToDoList";
import {ROUTE} from "./constants";

class App extends Component {
    render() {
        return (
            <Router>
                <ToDoApp/>
                <Switch>
                    <Route exact path={ROUTE.DEFAULT} component={PendingToDoList}/>
                    <Route exact path={ROUTE.COMPLETED_TODO_LIST} component={CompletedToDoList}/>
                    <Route exact path={ROUTE.PENDING_TODO_LIST} component={PendingToDoList}/>
                </Switch>
            </Router>
        );
    }
}

export default App;
