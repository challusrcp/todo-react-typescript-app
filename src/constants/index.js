export const APP_CONSTANTS={
    TODO_LIST:'todoList'
};

export const ROUTE={
    DEFAULT:'/',
    COMPLETED_TODO_LIST:'/completed',
    PENDING_TODO_LIST:'/pending'
    };

export const STATUS={
    COMPLETED:'completed',
    PENDING:'pending',
    DELETED:'deleted'
};

export const TODO={
NAME:'name',
    TODO_STATUS:STATUS.PENDING
};


