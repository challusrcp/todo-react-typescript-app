import React, {StatelessComponent} from "react";
import {ToDo} from "../models/ToDo";

export interface NewTaskFormProps {
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    onAdd: (event: React.FormEvent<HTMLFormElement>) => void;
    onCancel: () => void;
    toDo: ToDo;
}

export const NewToDoForm: StatelessComponent<NewTaskFormProps> = ({
                                                                      onChange,
                                                                      onAdd,
                                                                      toDo,
                                                                      onCancel,
                                                                  }) => (
    <form onSubmit={onAdd}>
        <input className="form-control" onChange={onChange} placeholder="Add todo...." value={toDo.name}/>
        <div>
            <button type="submit" onClick={(event: any) => onAdd(event)} className="btn btn-primary todo-btn">Submit
            </button>
            <button type="button" className="btn btn-danger todo-btn" onClick={onCancel}>Cancel</button>
        </div>

    </form>
);
