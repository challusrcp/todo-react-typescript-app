import React, {StatelessComponent} from "react";
import {ToDo} from "../models/ToDo";
import {ToDoListItem} from "./ToDosListItem";

export interface ToDoListProps {
    toDos: ToDo[];
    onDelete: (task: ToDo) => void;
    onComplete: (task: ToDo) => void;
}

export const ToDosList: StatelessComponent<ToDoListProps> = ({
                                                                 toDos,
                                                                 onDelete,
                                                                 onComplete
                                                             }) => (
    <ul>
        {toDos.map((toDo, key) => (
            <ToDoListItem key={key} toDo={toDo} onDelete={onDelete} onComplete={onComplete}/>
        ))}
    </ul>
);
