import React, {Component} from "react";
import {withRouter} from 'react-router-dom'
import {ToDo} from "../models/ToDo";
import {NewToDoForm} from "./NewToDoForm";
import {APP_CONSTANTS, ROUTE, STATUS} from "../constants";

interface State {
    newToDo: ToDo;
}

interface Props {
    history: any;
    onAdd:()=>void;
}


class AddToDo extends Component<Props, State> {
    state = {
        newToDo: {
            id: new Date().getTime(),
            name: "",
            status: STATUS.PENDING,
        },
    };

    private addToDo = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        let newToDo = this.state.newToDo;
        if (newToDo.name.trim().toString()) {
            let list: any = localStorage.getItem(APP_CONSTANTS.TODO_LIST);
            if (list && list.length > 0)
                list = JSON.parse(list);
            else
                list = [];
            localStorage.setItem(APP_CONSTANTS.TODO_LIST, JSON.stringify([...list, newToDo]));
            this.props.onAdd();
            this.setState({
                newToDo: {
                    id: new Date().getTime(),
                    name: "",
                    status: STATUS.PENDING,
                }
            });
        }

    };

    private handleToDoChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            newToDo: {
                ...this.state.newToDo,
                name: event.target.value
            }
        });
    };

    render() {
        return (
            <div>
                <div className="header todo-header">
                    <h2>Add To Do </h2>
                    <NewToDoForm
                        toDo={this.state.newToDo}
                        onAdd={this.addToDo}
                        onChange={this.handleToDoChange}
                        onCancel={this.props.onAdd}
                    />
                </div>
            </div>
        );
    }

}

export default withRouter<any, any>(AddToDo);
