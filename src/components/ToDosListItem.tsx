import React, {StatelessComponent} from "react";
import {ToDo} from "../models/ToDo";
import {STATUS} from "../constants";

export interface ToDoListItemProps {
    toDo: ToDo;
    onDelete: (task: ToDo) => void;
    onComplete: (task: ToDo) => void;
}

export const ToDoListItem: StatelessComponent<ToDoListItemProps> = ({
                                                                        toDo,
                                                                        onDelete,
                                                                        onComplete
                                                                    }) => {
    return (
        <li className={`${toDo.status === STATUS.COMPLETED && 'checked'} d-flex justify-content-between`}>
            <span>{toDo.name}</span>
            <span className="actions w-30">
          <i className="fa fa-check mr-20" onClick={() => onComplete(toDo)} aria-hidden="true"/>
          <span className="close w-30" onClick={() => onDelete(toDo)}>x</span>
            </span>
        </li>

    );
};
