import React, {Component} from "react";
import {withRouter} from 'react-router-dom'
import {ToDo} from "../models/ToDo";
import {APP_CONSTANTS, STATUS} from "../constants";

interface State {
    toDoList: ToDo[];
    showClear: boolean;
}

class CompletedToDoList extends Component<{}, State> {
    state = {
        toDoList: [],
        showClear: false
    };

    componentDidMount(): void {
        const store = localStorage.getItem(APP_CONSTANTS.TODO_LIST);
        if (store && store.length > 0) {
            let toDos = JSON.parse(store);
            if (toDos && toDos.length > 0 && !toDos.find((todo: any) => (todo.status === STATUS.PENDING))) {
                this.setState({
                    toDoList: toDos,
                    showClear: true
                })
            } else {
                this.setState({toDoList: toDos})
            }
        }
    }

    private clearToDos(): void {
        localStorage.clear();
        this.setState({
            toDoList: [],
            showClear: false
        })
    }


    render() {
        let {showClear, toDoList} = this.state;
        toDoList = toDoList.filter((todo: any) => todo.status === STATUS.COMPLETED);

        return (
            <div>
                <ul>
                    {showClear && <li className="d-flex flex-row-reverse">
                        <i className="fa fa-times-circle clear-icon"
                           data-toggle="tooltip"
                           data-placement="bottom"
                           title="Clear ToDo List"
                           onClick={this.clearToDos.bind(this)}/>
                    </li>}
                    {toDoList.length > 0 &&
                    toDoList.map((todo: any, key) => <li className='checked' key={key}>
                        {todo.name}
                    </li>)}
                </ul>
            </div>
        );
    }

}

export default withRouter<any, any>(CompletedToDoList);
