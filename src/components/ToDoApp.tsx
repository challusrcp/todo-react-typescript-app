import React, {Component} from "react";
import {Link, withRouter} from 'react-router-dom'
import {ROUTE} from "../constants";

class ToDoApp extends Component {
    render() {
        return (
            <div className="d-flex justify-content-around bg-warning py-3">
                <Link to={ROUTE.PENDING_TODO_LIST}> Pending ToDos</Link>
                <Link to={ROUTE.COMPLETED_TODO_LIST}> Completed ToDos </Link>
            </div>
        );
    }
}

export default withRouter<any, any>(ToDoApp);
