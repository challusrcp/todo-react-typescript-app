import React, {Component} from "react";
import {withRouter} from 'react-router-dom'
import {ToDo} from "../models/ToDo";
import {ToDosList} from "./ToDosList";
import {APP_CONSTANTS, ROUTE, STATUS} from "../constants";
import AddToDo from "./AddToDo";

interface State {
    isAdd: boolean;
    toDoList: ToDo[];
}

interface Props {
    history: any
}

class PendingToDoList extends Component<Props, State> {
    state = {
        isAdd: false,
        toDoList: [],
    };

    componentDidMount(): void {
        this.setData()
    }

    private setData = () => {
        let todoList = [];
        const store = localStorage.getItem(APP_CONSTANTS.TODO_LIST);
        if (store && store.length > 0) {
            todoList = JSON.parse(store);
            this.setState({
                toDoList: todoList
            })
        }
        /* if (!todoList.find((todo: any) => (todo.status===STATUS.PENDING)) && todoList.find((todo: any) => (todo.status===STATUS.COMPLETED))) {
             this.props.history.push(ROUTE.COMPLETED_TODO_LIST)
         }*/
    };

    private completeToDo = (toDoToComplete: ToDo) => {
        let toDoList = this.state.toDoList.map((toDo: any) => {
            if (toDo.id === toDoToComplete.id)
                toDo.status = STATUS.COMPLETED;
            return toDo
        });
        localStorage.setItem(APP_CONSTANTS.TODO_LIST, JSON.stringify(toDoList));
        if (!toDoList.find((todo: any) => (todo.status === STATUS.PENDING))) {
            this.props.history.push(ROUTE.COMPLETED_TODO_LIST)
        }
        this.setState({
            toDoList: toDoList
        });
    };

    private deleteToDo = (toDoToDelete: ToDo) => {
        let toDoList = this.state.toDoList.map((toDo: any) => {
            if (toDo.id === toDoToDelete.id)
                toDo.status = STATUS.DELETED;
            return toDo
        });
        localStorage.setItem(APP_CONSTANTS.TODO_LIST, JSON.stringify(toDoList));
        if (!toDoList.find((todo: any) => (todo.status === STATUS.PENDING))) {
            this.props.history.push(ROUTE.COMPLETED_TODO_LIST)
        }
        this.setState({
            toDoList: toDoList
        });
    };

    private addToDo = () => {
        this.setData();
        this.handleAddEvent();
    };

    private handleAddEvent = () => {
        this.setState({
            isAdd: !this.state.isAdd
        })
    };

    render() {
        let toDos = this.state.toDoList.filter((todo: any) => todo.status === STATUS.PENDING);
        return (
            <div>
                {this.state.isAdd ? <AddToDo onAdd={this.addToDo}/> :
                    <div className="d-flex flex-row-reverse add-icon-div pr-20">
                        <i onClick={this.handleAddEvent}
                           className="fa fa-plus-circle "
                           data-placement="bottom"
                           data-toggle="tooltip" title="Add ToDo"/>
                    </div>}
                <ul className="todo-ul">
                    {toDos.length > 0 &&
                    <ToDosList toDos={toDos}
                               onDelete={this.deleteToDo}
                               onComplete={this.completeToDo}/>}
                </ul>
            </div>
        );
    }

}

export default withRouter<any, any>(PendingToDoList);
