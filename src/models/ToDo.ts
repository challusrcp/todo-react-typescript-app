export interface ToDo {
  id: number;
  name: string;
  status:string;
}
